class Message < ApplicationRecord
  belongs_to :user
  belongs_to :room

  validates :body, :room, :user, presence: true
end
