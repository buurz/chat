class User < ApplicationRecord
  has_many :messages, dependent: :destroy

  validates :name, uniqueness: true

  enum status: [:online, :offline]
end
