class UsersService
  attr_reader :user, :error

  def initialize(user = nil)
    @user = user
  end

  def self.get_users
    users = REDIS.get 'users'

    u = new
    users ||= u.update_users
  end

  def create_user
    error!(user.errors.messages) unless user.save

    #TODO: async
    update_users
    true
  end

  def update_status(status)
    status == 'online' ? user.online! : user.offline!

    update_users
    nil
  end

  private

  def error!(message)
    @error = message
    nil
  end

  def update_users
    users = User.all.to_json
    REDIS.set("users", users)

    users = JSON.load users
  end
end
