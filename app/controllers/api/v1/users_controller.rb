module Api::V1
  class UsersController < ApplicationController

    def index
      users = UsersService.get_users

      render json: users, status: 200
    end

    def create
      @user = User.new(users_params)
      
      service = UsersService.new(@user)
      service.create_user

      if service.error.blank?
        render json: { message: 'success' }, status: 201
      else
        render json: { error: service.error }, status: 422
      end
    end

    private

    def users_params
      params.require(:user).permit(:id, :status, :name)
    end
  end
end
