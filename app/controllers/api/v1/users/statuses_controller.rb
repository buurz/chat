module Api
  module V1
    class Users::StatusesController < ApplicationController
      before_action :set_user

      def show
        user_status = @current_user.status

        render json: { status: user_status }, status: 200
      end

      def update
        service = UsersService.new(@current_user)

        service.update_status(params[:status])

        if service.error.blank?
          render json: { message: 'success' }, status: 201
        else
          render json: { error: service.error }, status: 422
        end
      end

      private

      def set_user
        @current_user ||= User.find(params['id'])
      end
    end
  end
end
