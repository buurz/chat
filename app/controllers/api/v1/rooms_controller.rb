module Api::V1
  class RoomsController < ApplicationController
    def create
    end

    def show
      @room = Room.find(params[:id])
      render json: @room.messages, status: 200
    end

    def destroy
    end

    def index
      @rooms = Room.all

      render json: @rooms, status: 200
    end
  end
end
