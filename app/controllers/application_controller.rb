class ApplicationController < ActionController::API
  include Authentication
  include ActionController::Cookies

  # protect_from_forgery with: :null_session
end
