class AppearanceChannel < ApplicationCable::Channel
  def subscribed
    stream_from "User_appearance"
  end

  def unsubscribed
    ActionCable.server.broadcast("User_appearance", params['data']['user'].merge(status: 'offline'))
  end

  def receive(data)
    ActionCable.server.broadcast("User_appearance", data['message']['user'])
  end
end
