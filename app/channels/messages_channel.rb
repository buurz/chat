class MessagesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "#{params[:data][:room]}"
  end

  def receive(data)
    ActionCable.server.broadcast("#{params[:data][:room]}", data['message'])

    # TODO: delay job
    message = Message.create data['message']
  end
end
