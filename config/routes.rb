Rails.application.routes.draw do
  mount ActionCable.server => "/api/cable"
  namespace :api, defaults: { format: 'json' } do
    scope module: :v1,
          constraints: ApiConstraints.new(version: 1, default: true) do

      get 'test' => 'application#index'

      resource :session

      resources :users do
        member do
          get 'status', to: 'users/statuses#show'
          put 'status', to: 'users/statuses#update'
        end
      end

      resources :rooms do
        resources :messages
      end
    end
  end
end
