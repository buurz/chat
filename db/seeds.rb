p 'Creating users'
names = %w(Василий Snoop 4ukky Iced)

names.each do |name|
  User.create! name: name unless User.find_by name: name
end

p 'Creating rooms'
froom = Room.create! title: 'Tha Shiznit', description: 'Poppin, stoppin, hoppin.', user: User.last
sroom = Room.create! title: 'ZipZip ', description: 'fap fap', user: User.first

p 'Creating messages'

Message.create! body: 'test', room: froom, user: User.last
Message.create! body: 'Second is here', room: froom, user: User.second
Message.create! body: 'First is here', room: froom, user: User.first
Message.create! body: 'Third is here too', room: froom, user: User.third

p 'Done'
