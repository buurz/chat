do ->

  OnlineController = ($rootScope, $scope, $http, $log, ActionCableChannel, $cookieStore, $filter) ->

    $scope.get_users = ->
      $http.get('api/users').success((data) ->
        $rootScope.users = data

        return
      ).error (data) ->
        $log.info 'err'+data
        return

    $scope.track_users = (user)->
      $scope.check_status(user)
      return user.id

    $scope.check_status = (user)->
      $http.get('api/users/'+user.id+'/status').success((data) ->

      ).error (data) ->
        $log.info "error_check_status "+JSON.stringify(data)
        return

    $scope.update_status = (user, status)->
      angular.merge(user, { status: status } )

      options = {
        status: status
      }
      $http.put('api/users/'+user.id+'/status', options)
        .success (data) ->
          res = $filter('filter')($scope.users, {id: user.id})[0]
          res.status = status
        .error (data) ->
          $log.info "error_update_status "+JSON.stringify(data)
          return

    $scope.$on 'appearUser', (event, currentUser) ->
      user = currentUser

      $scope.update_status(user, 'online')
      options = {
        user: {
          id: user.id
          name: user.name
        }
      }

      consumer = new ActionCableChannel("AppearanceChannel", options)

      callback = (message)->
        $log.info 'You are welcome: '+JSON.stringify(message)

        res = $filter('filter')($rootScope.users, {id: message.id})[0]
        index = $scope.users.indexOf(res)
        $rootScope.users.splice(index, 1)
        $rootScope.users.push message

      consumer.subscribe(callback).then(->
        consumer.send(user: user)

        $scope.$on("$destroy", ->
          $scope.update_status(user, 'offline')

          consumer.unsubscribe().then(->
          )
        )
      )
    return

  'use strict'
  angular.module('angRailsSkeleton').controller 'OnlineController', OnlineController
  return
