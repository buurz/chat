do ->
  RoomsController = ($rootScope, $scope, $http, $log, $cookieStore) ->
    $http.get('api/rooms').success((data) ->
      $scope.rooms = data

      $scope.rooms_empty = $.isEmptyObject(data)
      return
    ).error (data) ->
      $log.info data
      return

    $scope.current_room = (room_title) ->
      if $rootScope.currentRoom
        return $rootScope.currentRoom.title == room_title

    $scope.fire = (room) ->
      user = $cookieStore.get('loggedUser')

      $rootScope.$broadcast 'getMessagesFor', room
      $rootScope.currentRoom = room

      $rootScope.$broadcast 'appearUser', user
      $rootScope.currentUser = user

  'use strict'
  angular.module('angRailsSkeleton').controller 'RoomsController', RoomsController
  return
