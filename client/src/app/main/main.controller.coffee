angular.module 'angRailsSkeleton'
  .controller 'MainController', ($scope, $log, $cookieStore, $timeout, webDevTec, toastr) ->
    'ngInject'
    vm = this
    activate = ->
      getWebDevTec()
      $timeout (->
        vm.classAnimation = 'rubberBand'
        return
      ), 4000
      return

    showToastr = ->
      toastr.info 'Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>'
      vm.classAnimation = ''
      return

    getWebDevTec = ->
      vm.awesomeThings = webDevTec.getTec()
      angular.forEach vm.awesomeThings, (awesomeThing) ->
        awesomeThing.rank = Math.random()
        return
      return

    # $scope.isLive = cable.client.connection.isOpen();
    # TODO: Check the connection heartbeat
    $scope.isLive = true
    $scope.selected = $cookieStore.get('loggedUser')

    vm.awesomeThings = []
    vm.classAnimation = ''
    vm.creationDate = 1464030745279
    vm.showToastr = showToastr
    activate()
    return
