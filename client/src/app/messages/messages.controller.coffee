do ->

  MessagesController = ($rootScope, $scope, $http, $log, toaster, ActionCableChannel, $cookieStore) ->

    user = $cookieStore.get('loggedUser')
    room =
      id: 0
      title: ''

    $scope.$on 'getMessagesFor', (event, currentRoom) ->
      room = currentRoom
      $http.get('api/rooms/' + String(room.id)).then (data) ->
        $scope.messages = data.data
        # response data
        toaster.pop 'success', 'Entered the room', 'Successfully logged into ' + String(room.title)
        return
      # Open the socket connection
      consumer = new ActionCableChannel("MessagesChannel", room: "chat_#{room.title}")

      callback = (message)->
        $log.info 'Got a new message: '+message.body
        $scope.messages.push(message)

      consumer.subscribe(callback).then(->
        $scope.submit = (form) ->
          consumer.send(user_id: user.id, room_id: room.id, body: form.body)
          form.body = ''
        $scope.$on("$destroy", ->
          consumer.unsubscribe().then(->
            $scope.submit = undefined
          )
        )
      )

      return
    return

  'use strict'
  angular.module('angRailsSkeleton').controller 'MessagesController', MessagesController
  return
