do ->
  'use strict'
  angular.module('angRailsSkeleton').controller 'ModalsController', ($rootScope, $scope, $uibModal, $log, $http, toaster, $cookieStore) ->
    # Get cookie
    loggedUserCookie = $cookieStore.get('loggedUser')
    if loggedUserCookie
      $scope.isUserLoggedIn = true
      return loggedUserCookie
    else
      $scope.isUserLoggedIn = false
      $cookieStore.remove 'loggedUser'

    $scope.logout = ->
      if loggedUserCookie
        $http.delete('api/session', user_id: loggedUserCookie.id).then ((data) ->
          $log.info data
          # Remove the current logged in user from the cookie store
          $cookieStore.remove 'loggedUser'
          $scope.isUserLoggedIn = false
          $rootScope.currentRoom = ''
          toaster.pop 'success', 'Successfully logged out', 'You are now logged out!'
          return
        ), (data) ->
          $log.info data
          toaster.pop 'error', 'Couldn\'t log out!', 'Please try again!'
          return
        return
      else
        $scope.isUserLoggedIn = false
        $cookieStore.remove 'loggedUser'

    #show login at start
    $scope.auth_reg_flag = true

    $scope.login = (size) ->
      $http.get('api/users').success((data) ->
        $scope.users = data
        $rootScope.users = data
        # response data
        if $scope.users
          modalInstance = $uibModal.open(
            animation: true
            templateUrl: 'app/modals/modalContent.html'
            controller: 'ModalInstanceController'
            size: size
            resolve: users: ->
              $scope.users
          )
          modalInstance.result.then ((selectedItem) ->
            $scope.selected = selectedItem
            $scope.isUserLoggedIn = true
            return
          ), ->
            $log.info 'Modal dismissed at: ' + new Date
            return
        else
          toaster.pop 'error', 'Can\'t get users data from the server', 'Please check the connection'
        return
      ).error (data) ->
        $log.info data
        toaster.pop 'error', 'Couldn\'t login!', 'Please try again!'
        return
      return

    return

  angular.module('angRailsSkeleton').controller 'ModalInstanceController', ($rootScope, $scope, $uibModalInstance, users, toaster, $cookieStore, $http, $log) ->
    $scope.users = users
    if $scope.users
      $scope.selected = user: $scope.users[0]
    else
      $scope.selected = user: null

    $scope.createUser = (form)->
      options = {
        name: form.name
      }

      $http.post("api/users", options).then ( (success)->
        $uibModalInstance.close success.data.user
        $scope.selected = user: success.data.user
        $cookieStore.put 'loggedUser', $scope.selected.user
        $scope.ok()
          # Save the selected user into a cookie
      ),(error) ->
        toaster.pop 'error', 'Такой пользователь уже есть', 'Пожалуйста выберите другое имя!'

    $scope.ok = ->
      $http.post('api/session', user_id: $scope.selected.user.id).then ((data) ->

        $log.info data
        $uibModalInstance.close $scope.selected.user
        $scope.selected = user: $scope.selected.user
        # Save the selected user into a cookie
        $cookieStore.put 'loggedUser', $scope.selected.user
        toaster.pop 'success', 'Successfully logged in', 'Logged in as ' + String($scope.selected.user.name)
        return
      ), (data) ->
        $log.info data
        toaster.pop 'error', 'Couldn\'t login!', 'Please try again!'
        return
      return

    $scope.cancel = ->
      $uibModalInstance.dismiss 'cancel'
      toaster.pop 'error', 'You canceled the login', 'Please do login, to try the app '
      return

    return
  return
