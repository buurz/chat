app = angular.module 'angRailsSkeleton', ['ngAnimate',
'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria',
'ngResource', 'ui.router', 'toastr', 'ui.bootstrap', 'toaster', 'ngActionCable', 'angular-toArrayFilter']

app.config(['$httpProvider', ($httpProvider) ->
        $httpProvider.defaults.useXDomain = true
])
